﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MolecularController : MonoBehaviour
{
    //public events for changing the appearance of the game object
    public UnityEvent onHighlight;
    public UnityEvent onUnhighlight;

    public bool _isHighlighted;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseUp()
    {
        GameManager.Instance.HighlightMolecule(this);
    }

    public void Highlight()
    {
        onHighlight.Invoke();
        _isHighlighted = true;
    }

    public void Unhighlight()
     {
        onUnhighlight.Invoke();
        _isHighlighted = false;
    }

}
