﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LabelController : MonoBehaviour
{
    public Transform target;
    public RectTransform rect;
    public Text text; 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        rect.WorldToAnchoredPosition(target);
        if(target.IsOccluded())
        {
            text.enabled = false;
        }
        else
        {
            text.enabled = true;
        }
    }
}
